use strict;
use warnings;

print "type q or quit to exit\n";
print "Enter your input:";
my $input = <>;#reading input from the user
chomp $input;#removes the new line character at the end of the inputwhen pressing enter

#my $regex = '^\d+\.\d+$';
my $regex = 'A..B'#having A and B no ^ so not indeed at the start

until(($input eq "quit")||($input eq "q")) {
	if($input =~ /$regex/) { print "ACCEPTED\n"; }
	else { print "FAILED\n";}
	print "type q or quit to exit\n";
	print "Enter your input:";
	$input = <>;
	chomp $input;
}

#^expecting a string that starts wit \d digit char + has at least \. for real dott \at least ond digit $ ende with digit (catching floatin point no)
