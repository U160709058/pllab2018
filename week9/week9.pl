use strict;
use warnings;

print "type q or quit to exit\n";
print "Enter your input:";
my $input = <>;#reading input from the user
chomp $input;#removes the new line character at the end of the inputwhen pressing enter

my $regex = '^\d+\.\d+$';
#^expecting a string that starts wit \d digit char + has at least \. for real dott \at least ond digit $ ende with digit (catching floatin point no)
my $regex = 'A..B'
#having A and B no ^ so not indeed at the start
#A.+B if we enter AB it fails cuz we're expecting at least one character between them
my $regex = '^\w+@"{[a-z]+\.}+[a-z]{2,}'
#to capture an email address w specifies just chars or digits or _ then @ then just chars \. real dott then just chars but at least 2 .tr
#if we put $ at the end it means we only accept 2 chars at the end posta.mu.tr is not accepted
#{...}+ the hole pattern can be repeated

until(($input eq "quit")||($input eq "q")) {
	if($input =~ /$regex/) { print "ACCEPTED\n"; }
	else { print "FAILED\n";}
	print "type q or quit to exit\n";
	print "Enter your input:";
	$input = <>;
	chomp $input;
}


 
