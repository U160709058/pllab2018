use strict;
use warnings;

hello();

my $avg = average(10,20,30);
print "Average of 10,20 and 30 is equal to $avg. \n";

print "Enter a DNA sequence";
my $DNA = <>;
chomp $DNA;
$DNA = myfunc($DNA);
print "$DNA \n";

print "Enter a DNA sequence";
my $DNA2 = <>;
chomp $DNA2;
myfunc(\$DNA2);
print "$DNA2 \n";

sub hello {
    print "Hello World! \n";
}

sub average(){
    my $size = scalar @_;
    my $total = 0;
    for(my $i=0 ; $i<$size ; $i++){
        $total += $_[$i];
    }
    my $avg = $total / $size;
    return $avg;
}

sub myfun{
    my ($DNA) = @_;
    if($DNA =~ /atg/){
        $DNA =~ s/atd/ATG/g;
    }
    return $DNA;
}

sub myfun2{
    my $DNAref = shift;
    if($$DNAref =~ /atg/){
        $$DNAref =~ /s/atd/ATG/g;
    }
}



