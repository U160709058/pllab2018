use strict;
use warnings;

print "Hello this is my first Perl script\n";

my $scalar_variable = 5;

print 'The value of $scalar_variable is' . " $scalar_variable" . "\n";

$scalar_variable = "This is a String";

print 'The value of $scalar_variable is now ' . " $scalar_variable" . "\n";

my @a = (10,20,30);

print "@a\n";

@a = (10 , "This is a String " , 4.21 , $scalar_variable);

print "@a\n";
