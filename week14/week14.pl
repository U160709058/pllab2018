use strict;
use warnings;

my $file = $ARGV[0];
open IN, '<', $file or die "cannot open $file : $!\n";
my @lines = <IN>;
close IN;

my %words_counts = ();
foreach my $line (@lines){
    chomp $line;
    my @row = split(/\s/,$line);
    foreach my $word (@row){
        if(exists $words_counts{$word}) {$words_counts{$word} += 1;}
        else{$words_counts{$word} = 1;}
    }
}

my @words = keys %words_counts;
foreach my $w (@words){
    print "$w -> $words_counts{$w}\n";
}

